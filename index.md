---
title: Procesamiento de Datos con Kotlin
date: 26 de Febrero del 2019
progres: true
theme: white
history: true
center: false
logo: images/kotlin_logo.svg
css:
  - css/overrides.css
---

# Qué vamos a ver

* El proyecto
* Los desafíos técnicos
* El análisis de los datos

# El proyecto

* Armar una red de empresas
* Mostrar las conexiones entre empresas y socias

---

![Vista Empresa](images/profile-empresa.svg)

---

![Vista Socia](images/profile-socia.svg)

---

![Data Science](images/data_science.png)

# Los desafíos técnicos

* Usar una API no documentada
* Entender la organización de la información
* Extraer datos de texto desestructurado
* Leer las entradas de todos los años y guardarlas en una BD


# Entender la organización de la información

* Inferir qué conceptos existen
* Determinar las conexiones entre los conceptos
* Generar un modelo de datos genérico

# Organización de una sección del BORA

![BORA Model](images/bora_model.svg)


# en JSON

```json
{
  "id": "8EH7GzTb2f7A5p9VmBschC1bLS1JVi1bLaSmtui53/8k3/sABsK9g4M=",
  "dataList": [
    [
      {
        "id": "A815886",
        "anioTramite": null,
        "numeroTramite": null,
        "rubro": "CONSTITUCION SA",
        "rubroPadre": "SOCIEDADES ANONIMAS",
        "subTitulo": null,
        "titulo": null,
        "nuevo": true,
        "fechaPublicacion": null,
        "denominacion": "ESTABLECIMIENTO AGROPECUARIO DON CARLOS S.A.",
        "idAviso": null,
        "tieneAnexos": false
      }
    ],
    "2019022202N.pdf",
    null,
    "20190222",
    "1491"
  ]
}
```

# en Kotlin

```kotlin
data class Page(
  val sessionId: String,
  val items: List<PageItem>
)

data class PageItem(
  val fileId: String,
  val description: String,
  val category: String,
  val parentCategory: String,
  val hasAttachments: Boolean = false
)

data class SectionFile(
  val id: String,
  val pdfFile: String,
  val publicationDate: String,
  val categoryId: String,
  val category: String,
  val text: String
)
```

# Data classes

* Estructura de datos diseñada especialmente para guardar datos
* La identidad es automática y está definida por el conjunto todos los campos
* Son inmutables, para cambiar un dato hay que copiarla usando el método copy()

# Data classes

```kotlin
val item1 = PageItem(
  fileId = "A815886",
  description = "ESTABLECIMIENTO AGROPECUARIO DON CARLOS S.A.",
  category = "CONSTITUCION SA",
  parentCategory = "SOCIEDADES ANONIMAS",
  hasAttachments = false
)

val item2 = PageItem(
  fileId = "A815886",
  description = "ESTABLECIMIENTO AGROPECUARIO DON CARLOS S.A.",
  category = "CONSTITUCION SA",
  parentCategory = "SOCIEDADES ANONIMAS",
  hasAttachments = false
)

// prints true
println(item1 == item2)
```

# Extraer datos de texto desestructurado

* Detectar patrones que se repitan e inferir una estructura (tokenizar)
* Clasificar la estructura (taggear)
* Extraer y estructurar datos (parsear)

# Tokenizar

![Tokenizer](images/tokenizer_01.svg)

# Tokenizer - Algoritmo

* Recorrer texto caracter por caracter
    * Si el caracter actual es un número, guardarlo porque **puede ser** el comienzo de un token
    * Si el caracter actual es un paréntesis de cierre y hay almacenado un número, **es** el comienzo de un token
    * Si no se cumple ninguna condición anterior, **borrar la lista** de caracteres guardados
    * Al llegar al final del texto, **cerrar** el último token abierto

# Tokenizer en kotlin

```kotlin
override fun tokenize(document: ByteArray): List<Token> {
    return DocumentStream(document).fold(
        TokenizerContext.new()
    ) { context, character ->
        val isToken: Boolean = context.isNumeric()
          && character == CLOSING_PARENTHESIS
        val isCandidate: Boolean = context.isNumeric()
          || context.isEmpty()

        when {
            isToken -> context.collect(character).open(
                position = stream.position()
            )
            isCandidate -> context.collect(character)
            else -> context.reset().collect(character)
        }
    }.finish(document.size)
}

```

# Taggear

![Tagger](images/tagging_01.svg)

# Normalización

Aplicar transformaciones a un texto para lograr mayor uniformidad.

* Convertir todas las palabras a minúsculas
* Eliminar *palabras vacías* (stop words)
* Reducir las palabras a sus raíces (stemming)

---

![Scammer Aliens](images/futurama_information_aliens.png)

# El proyecto

![Vista Empresa](images/profile-empresa.svg)

# Extraer y estructurar datos

![Parsing Model](images/parsing_model.svg)

# Modelo en kotlin

```kotlin
data class CompanyInfo(
    val legalId: String,
    val fileId: String,
    val name: String,
    val creationDate: DateTime,
    val partners: List<Partner>,
    val constitution: String?,
    val description: String?,
    val capital: String?,
    val duration: String?,
    val authorization: String?,
    val authorities: String?,
    val address: String?
)

data class Partner(val documentId: String)
```

# Extraer socias

```kotlin
class PartnersParser {

    companion object {
        private val PARTNERS_PARSER: Regex =
          Regex("DNI[\\s]*(\\d{1,3}\\.?\\d{3}\\.?\\d{3})")
    }

    fun parse(text: String): List<Partner> {
      return PARTNERS_PARSER.findAll(text).map { matchResult ->
          Partner(
              documentId = matchResult.groupValues[1]
                  .replace(".", "")
                  .trim()
          )
      }.distinctBy { partner ->
          partner.documentId
      }.toList()
    }
}
```

# Leer todo el BORA

* Detener y continuar el proceso de lectura
* No leer los datos ya leídos
* Identificar las fallas para reintentar más tarde

---

![](images/import_fan_out.svg)

